using UnityEngine;
using System.Collections;
using Undefine;
public class TagPlayerCentralView : TaggedObject {

    private static TagPlayerCentralView _instanceInScene;

    public static TagPlayerCentralView InstanceInScene
    {
        get { return _instanceInScene; }
        set { _instanceInScene = value; }
    }



    protected override void Awake() {
        base.Awake();
        _instanceInScene = this;
    }
    


}
