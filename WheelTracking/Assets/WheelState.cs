using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WheelState : MonoBehaviour {

    public enum WheelSide { Right, Left}
    public WheelSide _wheelSide;
    public Text _tAccelerometer;
    public Text _tWheelState;


    public Button _tWheelSwitchButton;
    public Text _tWheelSwitchButtonText;

    public float _wheelAngle;
    public Vector3 _initialAcceleration;
    void Start () {

        _initialAcceleration = Input.acceleration;
        _tWheelSwitchButton.onClick.AddListener( SwitchWheelSide);

    }
	
	void Update () {

        _tAccelerometer.text = ""+Input.acceleration;
        _tWheelState.text = "" + GetWheelAngle();
	}

    public float GetWheelAngle() {
        float angle=0;
        float x = Input.acceleration.x;
        float sign = Mathf.Sign(Input.acceleration.y);

        x *= _wheelSide == WheelSide.Right ? -1f : 1f;

        //-1 0 1 -> 0 1 2  =  (x+1)
        // 0 1 2 -> 0. 0.5 1   =  (x+1)/2f
        // 0 0.5 1 -> 1 0.5 0   = 1 - (...)
        // 0 0.5 1 -> 180 90 0 =  (1-(...)) *180
        angle = ((1f - ((x + 1f) / 2f)) * 180f*sign);
        return angle;
    }

    public void SwitchWheelSide()
    {
        if (_wheelSide == WheelSide.Left)
            _wheelSide = WheelSide.Right;
        else
            _wheelSide = WheelSide.Left;
        _tWheelSwitchButtonText.text = _wheelSide.ToString();


    }
}
