using UnityEngine;
using System.Collections;

public class WheelStateToUDP : MonoBehaviour {


    public WheelState _wheelState;
    public UDP_LinkSenderToServer _linkServer;
    public float _sendMessageDelay = 0.08f;
   
	void Start () {

        InvokeRepeating("SendWheelStateUDP", 0, _sendMessageDelay);
	
	}
    public void SendWheelStateUDP()
    {
        string cmd = 
            _wheelState._wheelSide == WheelState.WheelSide.Right ? "WR" : "WL";
        cmd +=":"+ _wheelState.GetWheelAngle();
        _linkServer.sender.Send(cmd);


    }
}
