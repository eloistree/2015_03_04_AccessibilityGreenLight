using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IsUserConnected : MonoBehaviour {

    public Text _tIsConnected;
    public UDP_AFK_Checker _linkChecker;
    
	// Update is called once per frame
	void Update () {
        bool isConnected = _linkChecker.IsConnected;

        _tIsConnected.text = isConnected ? "Connected":"Searching ...";
        _tIsConnected.color = isConnected ? Color.green : Color.red;

    }
}
