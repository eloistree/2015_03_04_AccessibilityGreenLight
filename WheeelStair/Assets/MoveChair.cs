using UnityEngine;
using System.Collections;
using System;

public class MoveChair : MonoBehaviour {

    public WheelChair _wheelChair;
    public Rigidbody _rigBody;
    public Transform _chairDirection;
    public Transform _ChairRoot;
    public float _force=2;
    public float _torqueForce=5;
    public ForceMode _forceMode = ForceMode.Impulse;
    public ForceMode _forceRotation=ForceMode.Force;
    public float _WheelRadius=1f;

    [Header("Debug")]
    public bool manual;
   
    void Update () {

		float left = (Input.GetKey(KeyCode.U) ? 0.3f : 0f) + (Input.GetKey(KeyCode.J) ? -0.3f  : 0f);
		float right = (Input.GetKey(KeyCode.O) ? 0.3f  : 0f) + (Input.GetKey(KeyCode.L) ? -0.3f  : 0f);


        if (left == 0f && right == 0f) { 
             left = _wheelChair.GetLeftWheelState();
             right = _wheelChair.GetRightWheelState();
        }
        float average = (left+right)/2f;
        //ReduceToMinimum(ref left, ref right);


        if (IsMovingFoward(left, right))
        {
            //_ChairRoot.Translate(Vector3.forward * average * Time.deltaTime * _force);
            _rigBody.AddForce(average*_chairDirection.forward * _force * Time.deltaTime, _forceMode);

        }
        else if (IsSomethinkMoving(left, right) ) {
            _rigBody.AddTorque(left * Vector3.up * _torqueForce * Time.deltaTime, ForceMode.Impulse);
            _rigBody.AddTorque(-right * Vector3.up * _torqueForce * Time.deltaTime, ForceMode.Impulse);


        }




    }

    private bool IsSomethinkMoving(float left, float right)
    {
        float minValue = 0.03f;
        return (left > minValue || right > minValue || left < -minValue || right < -minValue);
            
    }

    private bool IsMovingFoward(float left, float right)
    {
        if (! IsSomethinkMoving(left,right))
            return false;
        if (Math.Sign(left) != Math.Sign(right))
            return false;
        return true;
        
    }

    private void ReduceToMinimum(ref float left, ref float right)
    {
        if (Math.Sign(left) == Math.Sign(right))
        {
            float sign = Math.Sign( left );
            if (Math.Abs(left) > Math.Abs(right))
            {
                left = sign * (Math.Abs(left) - Math.Abs(right));
                right = 0f;
            }
            else {
                right = sign * (Math.Abs(right) - Math.Abs(left));
                left = 0f;
            }
        }
        else {
            if (Math.Abs(left) > Math.Abs(right))
            {
                left = Math.Sign(left) * (Math.Abs(left) + Math.Abs(right));
                right = 0f;
            }
            else {
                right = Math.Sign(right) * (Math.Abs(right) + Math.Abs(left));
                left = 0f;
            }
        }
    }
}
