using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UDPWheelState : MonoBehaviour {


    public UDP_Server _server;

    public float _leftWheelAngle;
    public float _rightWheelAngle;

    public Text _tLeftWheelDebug;
    public Text _tRightWheelDebug;

    void Start () {
        _server.receiver.onPackageReceived += ConvertMessageToWheelState;
	}

    private void ConvertMessageToWheelState(UDP_Receiver from, string message, string adresse, int port)
    {
        string[] tokens = message.Split(':');
        if (tokens.Length > 1 && tokens[0].StartsWith("WR"))
        {
            _rightWheelAngle = float.Parse(tokens[1]);
            if (_tRightWheelDebug != null)
                _tRightWheelDebug.text = string.Format("{0:0.00}", _rightWheelAngle);
        }
        if (tokens.Length > 1 && tokens[0].StartsWith("WL"))
        {
            _leftWheelAngle = float.Parse(tokens[1]);
            if (_tLeftWheelDebug != null)
                _tLeftWheelDebug.text = string.Format("{0:0.00}", _leftWheelAngle);
        }
    }

    public float GetWheelLeftAngle()
    {
        return _leftWheelAngle;
    }
    public float GetWheelRightAngle()
    {
        return _rightWheelAngle;
    }
}
