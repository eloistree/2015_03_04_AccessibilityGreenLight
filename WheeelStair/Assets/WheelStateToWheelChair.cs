using UnityEngine;
using System.Collections;

public class WheelStateToWheelChair : MonoBehaviour {

    public UDPWheelState _udpWheelstate;
    public WheelChair _wheelChair;

	void Update ()
    {
        _wheelChair.SetWheelLeftAngle(_udpWheelstate._leftWheelAngle);
        _wheelChair.SetWheelRightAngle(_udpWheelstate._rightWheelAngle);

    }
}
