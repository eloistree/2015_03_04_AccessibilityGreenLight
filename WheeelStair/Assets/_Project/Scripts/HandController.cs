using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {

	public Renderer m_Renderer;
	public Color m_FadeInColor;
	public Color m_FadeOutColor;
	public float m_FadeTime;

	public float m_HighLightSpeed;
	public Color m_HighLightColor;

	private Material m_HandMaterial;

	// Use this for initialization
	void Start () {
		m_HandMaterial = m_Renderer.material;
	}

	void Update (){

	}

	public void FadeOut (float startDelay) {
		StopAllCoroutines ();
		StartCoroutine (FadeToColor (startDelay, m_FadeTime, m_FadeOutColor, false, true));
	}

	public void FadeIn (float startDelay) {
		StopAllCoroutines ();
		StartCoroutine (FadeToColor (startDelay, m_FadeTime, m_FadeInColor, true, false));
	}

	public void HighLight (){
		StopAllCoroutines ();
		StartCoroutine (FadeToColor(0,  m_HighLightSpeed/2, m_HighLightColor, true, false));
		StartCoroutine (FadeToColor(m_HighLightSpeed/2,  m_HighLightSpeed/2, m_FadeInColor, true, false));
	}

	IEnumerator FadeToColor(float startDelay, float transitionSpeed, Color targetColor, bool enable, bool disable){		
		yield return new WaitForSeconds(startDelay);

		if(enable)
			m_Renderer.enabled = true;

		float transitionStartTime = Time.time;

		while (Mathf.Clamp((Time.time - transitionStartTime) / m_FadeTime,0,1) < 0.95) {
			m_HandMaterial.color = Color.Lerp (m_HandMaterial.color, targetColor, Mathf.Clamp ((Time.time - transitionStartTime) / transitionSpeed, 0, 1));
			yield return null;
		}

		if(disable)
			m_Renderer.enabled = false;
	}

}
