using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class InteractiveItem : MonoBehaviour {

    public UnityEvent m_OnHover;
    public bool m_Activated = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnHover()
    {
        if (m_Activated)
        {
            m_OnHover.Invoke();
        }
    }
}
