using UnityEngine;
using System.Collections;

public class FollowCameraPosition : MonoBehaviour {

	public Transform m_CenterEyeAnchor;
	public bool m_Easing = false;
	public float m_EasingFactor = 1;

	private Vector3 m_TargetPosition;
	private Quaternion m_TargetRotation;
	
	// Update is called once per frame
	void LateUpdate () {
		
		if (!m_Easing) 
		{
			transform.position = m_CenterEyeAnchor.position;
			transform.rotation = m_CenterEyeAnchor.rotation;
		} 
		else 
		{
			m_TargetPosition = m_CenterEyeAnchor.position;
			m_TargetRotation = m_CenterEyeAnchor.rotation;

			transform.position = Vector3.Lerp (transform.position, m_TargetPosition, Time.deltaTime * m_EasingFactor);
			transform.rotation = Quaternion.Lerp (transform.rotation, m_TargetRotation, Time.deltaTime * m_EasingFactor);
		}
	}
}
