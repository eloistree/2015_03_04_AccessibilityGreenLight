using UnityEngine;
using System.Collections;

public class PlayAnimation : MonoBehaviour {

    Animator m_Anim;
    public AnimationClip anim;

    // Use this for initialization
    void Start ()
    {
        m_Anim = GetComponent<Animator>();
	}

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Play();
        }
    }

    public void Play()
    {
        StartCoroutine(PlayCR());
    }

    IEnumerator PlayCR()
    {
        m_Anim.Play(anim.name);
        yield return new WaitForSeconds(anim.length);
    }
}
