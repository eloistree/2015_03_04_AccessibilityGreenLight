using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ReachController : MonoBehaviour {

	public Transform m_Chair;
	public Transform m_CenterEyeAnchor;
	public float m_SwitchHandAngle;
	//events
	public UnityEvent m_OnLookRight;
	public UnityEvent m_OnLookLeft;

	private bool lookingRight = false;
	private bool lookingLeft = false;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		// Find the direction the player is looking but on a flat plane.
		Vector3 lookDirection = Vector3.ProjectOnPlane (m_CenterEyeAnchor.forward, Vector3.up).normalized;
		float angle = Vector3.Angle ( lookDirection, m_Chair.forward);

		if (Vector3.Cross (lookDirection, m_Chair.forward).normalized.y == -1 && !lookingRight && angle > m_SwitchHandAngle) {
			lookingRight = true;
			lookingLeft = false;

			m_OnLookRight.Invoke ();

		} else if(Vector3.Cross (lookDirection, m_Chair.forward).normalized.y == 1 && !lookingLeft && angle > m_SwitchHandAngle){
			lookingLeft = true;
			lookingRight = false;

			m_OnLookLeft.Invoke ();
		}
	}
}
