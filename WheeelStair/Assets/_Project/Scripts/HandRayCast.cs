using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class HandRayCast : MonoBehaviour {

	public Transform m_Hand;
	public float m_TransitionFactor = 1f;
	public Transform m_Anchor;
	public bool m_Enabled = true;

	public UnityEvent m_OnHoverOn;

	private Vector3 m_OriginalPosition;
	private Vector3 m_HeadPosition;
	private bool m_HoverOnInteractive = false;
	private float m_Range;
	private Ray m_Ray;
	private RaycastHit m_Hit;
	private Vector3 m_TargetPosition;

	// Use this for initialization
	void Start () {
		m_OriginalPosition =  m_Hand.position;
		m_HeadPosition = m_Hand.parent.transform.position;

		m_Range = Vector3.Distance (m_OriginalPosition, m_HeadPosition);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_Enabled) {
			//setup raycast
			m_Ray.origin = m_HeadPosition;
			m_Ray.direction = m_Hand.position - m_HeadPosition;

			if (Physics.Raycast (m_Ray, out m_Hit, m_Range)) {

				//place cursor at hitpoint
				m_Hand.position = m_Hit.point;
				m_TargetPosition = m_Hit.point;

				//check if hit was interactive
				if (m_Hit.transform.gameObject.tag == "Interactive") {
					if (!m_HoverOnInteractive) {
						m_HoverOnInteractive = true;
						m_Hit.transform.gameObject.SendMessage ("OnHover");
						m_OnHoverOn.Invoke ();
					}
				} else {
					m_HoverOnInteractive = false;
				}
			} else {
				m_HoverOnInteractive = false;

				m_TargetPosition = m_Anchor.position;
			}

			m_Hand.position = Vector3.Lerp (m_Hand.position, m_TargetPosition, Time.deltaTime * m_TransitionFactor);
		}
	}

}
