using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WheelChair : MonoBehaviour
{

    public WheelInfo _leftWheel = new WheelInfo();
    public WheelInfo _rightWheel = new WheelInfo();

    public void Update()
    {
        _leftWheel.Update();
        _rightWheel.Update();
    }


    internal void SetWheelLeftAngle(float angle)
    {
        _leftWheel.SetAngle( angle);
    }

    internal void SetWheelRightAngle(float angle)
    {
        _rightWheel.SetAngle(angle);
    }

    internal float GetLeftWheelState()
    {
        return _leftWheel.GetWheelState();
    }
    internal float GetRightWheelState()
    {
        return _rightWheel.GetWheelState();
    }

    [System.Serializable]
    public class WheelInfo {
        public float _angle;
        public float _previousAngle;
        public float _angleDelta;
        public float _angleDeltaCutoff = 100f; // cap the delta angle between pos and neg of this value
        public float _deltaDifferenceCutoff = 100f; // cutoff for average delta difference

        public float _maxDelta = 40f;

        private Queue<float> _deltaList = new Queue<float>();
        


        public void Update()
        {

            float newDelta =  _angle - _previousAngle;
            _previousAngle = _angle;

            if (newDelta == 0)
                return;

            _deltaList.Enqueue(_angleDelta);
            if (_deltaList.Count > 10)
                _deltaList.Dequeue();

            if (Mathf.Abs(GetAverageDelta() - newDelta) > _deltaDifferenceCutoff)
                return;

            _angleDelta = Mathf.Round(newDelta);


        }

        private float GetAverageDelta()
        {
            float sum = 0;
            foreach (float delta in _deltaList)
            {
                sum += delta;
            }
            return (_deltaList.Count == 0) ? 0 : sum / _deltaList.Count;
        }

        internal void SetAngle(float angle)
        {
            _angle = angle;
        }
        internal float GetWheelState()
        {

            return - Mathf.Clamp(_angleDelta, -_maxDelta, _maxDelta) / _maxDelta;
        }
    }


}
