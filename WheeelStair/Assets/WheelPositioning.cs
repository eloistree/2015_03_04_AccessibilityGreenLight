using UnityEngine;
using System.Collections;

public class WheelPositioning : MonoBehaviour {

    public WheelChair _wheelChair;
    public Transform _leftWheel;
    public Transform _rightWheel;


    void Update ()
    {
        //_leftWheel.transform.localRotation = Quaternion.Euler(_wheelChair._wheelLeft._angle, 0, 0);
        //_leftWheel.transform.localRotation = Quaternion.Euler(_wheelChair._leftWheelAngle, 0, 0);
        _rightWheel.transform.localRotation = Quaternion.Euler(_wheelChair._rightWheel._angle, 0, 0);
        _leftWheel.transform.localRotation = Quaternion.Euler(_wheelChair._leftWheel._angle, 0, 0);


    }
}
