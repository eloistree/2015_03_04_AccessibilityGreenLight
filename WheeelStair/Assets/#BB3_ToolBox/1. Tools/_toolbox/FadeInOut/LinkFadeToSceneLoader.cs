using UnityEngine;
using System.Collections;

[RequireComponent(typeof(QuickFadeInOut))]
public class LinkFadeToSceneLoader : MonoBehaviour {

    private QuickFadeInOut fader;

    public void OnEnable() {
        fader = GetComponent<QuickFadeInOut>();
        LoadScene.OnStartLoadWithDelay += FadeOut;

    }

    private void FadeOut(string currentScene, string nextScene, float delay)
    {
        if (fader)
            fader.SetBlack();
    }
}
